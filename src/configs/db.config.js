module.exports = {
    mongo: {
        urlTest: process.env.DB_URL_TEST,
        urlDev: process.env.DB_URL_DEV
    }
}
