module.exports = {
    portTest: process.env.APP_PORT_TEST,
    portDev: process.env.APP_PORT_DEV,
    name: process.env.APP_NAME
}
