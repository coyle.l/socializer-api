const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userValidator = require("../utilities/validators/user.validator");

const userSchema = new Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
      lowercase: true
    },
    password: {
      type: String,
      required: true,
      minlength: 4,
      maxlength: 128
    },
    firstname: {
      type: String,
      required: true,
      maxlength: 50
    },
    lastname: {
      type: String,
      required: true,
      maxlength: 50
    },
    adresse: {
      type: String
    },
    telephone: {
      type: String
    },
    admin: {
      type: Boolean
    }
  },
  {
    timestamps: true
  }
);

userSchema.methods.joiValidate = obj => {
  const Joi = require("joi");
  let schema = userValidator.create;
  return Joi.validate(obj, schema);
};

module.exports = mongoose.model("User", userSchema);
