const express = require('express');
const router = express.Router();
const user = require('../controllers/user.controller');

// Create a new user
router.post('/users', user.create);

// find all users
router.get('/users', user.findAll);

// Find a single user with id
router.get('/users/:id', user.findOne);

// Update a user with id
router.put('/users/:id', user.update);

// Delete a user with id
router.delete('/users/:id', user.delete);

// Delete all users
router.delete('/users/', user.removeAll);

module.exports = router
