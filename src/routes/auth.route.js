const express = require("express");
const router = express.Router();
const auth = require("../controllers/auth.controller");

// register a user
router.post("/auth/register", auth.register);

// verify
router.get("/auth/sign", auth.sign);

// login
router.post("/auth/login", auth.login);

// logout
router.get("/auth/logout", auth.logout);

module.exports = router;
