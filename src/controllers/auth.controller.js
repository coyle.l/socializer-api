const jwt = require("jsonwebtoken");
const User = require("../models/user.model.js");
const bcrypt = require("bcryptjs");

exports.register = (req, res) => {
  const user = new User({
    email: req.body.email,
    password: req.body.password,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    adresse: req.body.adresse,
    telephone: req.body.telephone,
    admin: req.body.admin
  });

  let err = user.joiValidate(req.body);

  if (err.error) {
    res.send(err);
    console.log("joi error", err);
  } else {
    let hashedPassword = bcrypt.hashSync(req.body.password, 10);
    user.password = hashedPassword;
    User.create(user, function(err, user) {
      if (err) return res.status(500).send(err);

      // create a token
      let token = jwt.sign({ id: user._id }, "supersecret", {
        expiresIn: 86400
      });

      res.status(200).send({ auth: true, token: token });
    });
  }
};

exports.sign = (req, res, next) => {
  var token = req.headers["x-access-token"];
  if (!token)
    return res.status(401).send({ auth: false, message: "No token provided." });

  jwt.verify(token, "supersecret", function(err, decoded) {
    if (err)
      return res
        .status(500)
        .send({ auth: false, message: "Failed to authenticate token." });

    User.findById(decoded.id, { password: 0 }, function(err, user) {
      if (err)
        return res.status(500).send("There was a problem finding the user.");
      if (!user) return res.status(404).send("No user found.");

      next(user);
    });
  });
};

exports.login = (req, res) => {
  console.log("login user email", req.body.email);
  User.findOne({ email: req.body.email }, function(err, user) {
    if (err) return res.status(500).send("Error on the server.");

    if (!user) return res.status(404).send("No user found.");

    if (bcrypt.compareSync(req.body.password, user.password)) {
      let token = jwt.sign({ id: user._id }, "supersecret", {
        expiresIn: 86400
      });
      return res.status(200).send({ auth: true, token: token });
    } else {
      return res.status(404).send("Wrong password");
    }
  });
};

exports.logout = (req, res) => {
  res.status(200).send({ auth: false, token: null });
};
