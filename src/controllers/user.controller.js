const User = require("../models/user.model.js");
const Joi = require("joi");
const bcrypt = require("bcryptjs");

// Create and Save a new user
exports.create = (req, res) => {
  const user = new User({
    email: req.body.email,
    password: req.body.password,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    adresse: req.body.adresse,
    telephone: req.body.telephone,
    admin: req.body.admin
  });

  let err = user.joiValidate(req.body);

  if (err.error) {
    res.send(err);
    console.log("joi error", err);
  } else {
    let hashedPassword = bcrypt.hashSync(req.body.password, 10);
    user.password = hashedPassword;
    console.log("user password", user.password);
    user
      .save()
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: err.message || "Some error occurred while creating the user."
        });
      });
  }
};

// Find and return all User from the database.
exports.findAll = (req, res) => {
  User.find()
    .then(users => {
      res.send(users);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while finding users."
      });
    });
};

// Find a single user with id
exports.findOne = (req, res) => {
  User.findById(req.params.id)
    .then(user => {
      if (!user) {
        return res.status(404).send({
          message: "user not found with id " + req.params.id
        });
      }
      res.send(user);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "User not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error retrieving user with id " + req.params.id
      });
    });
};

// Update a user
exports.update = (req, res) => {
  let err = user.joiValidate(req.body);
  if (err.error) {
    res.send(err);
  } else {
    User.findByIdAndUpdate(
      req.params.id,
      {
        email: req.body.email,
        password: req.body.password,
        firstname: req.body.firstname,
        adresse: req.body.adresse,
        telephone: req.body.telephone,
        admin: req.body.admin
      },
      { new: true }
    )
      .then(user => {
        if (!user) {
          return res.status(404).send({
            message: "User not found with id " + req.params.id
          });
        }
        res.send(user);
      })
      .catch(err => {
        if (err.kind === "id") {
          return res.status(404).send({
            message: "user not found with id " + req.params.id
          });
        }
        return res.status(500).send({
          message: "Error updating user with id " + req.params.id
        });
      });
  }
};

// Delete a user with the specified userid in the request
exports.delete = (req, res) => {
  User.findByIdAndRemove(req.params.id)
    .then(user => {
      if (!user) {
        return res.status(404).send({
          message: "User not found with id " + req.params.id
        });
      }
      res.send({ message: "User deleted successfully!" });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "user not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Could not delete user with id " + req.params.id
      });
    });
};

exports.removeAll = (req, res) => {
  User.deleteMany({})
    .then(user => {
      res.send("all users are removed");
    })
    .catch(err => {
      if (err) {
        res.send(err);
      }
    });
};
