const mongoose = require("mongoose");
const config = require("../configs");

exports.connect = env => {
  let url;
  if (env == "test") {
    url = config.database.mongo.urlTest;
  } else if (env == "dev") {
    url = config.database.mongo.urlDev;
  } else {
    url = config.database.mongo.urlDev;
  }
  mongoose
    .connect(url, { useNewUrlParser: true, useCreateIndex: true })
    .then(() => {
      console.log("Successfully connected to the database");
    })
    .catch(err => {
      console.log("Could not connect to the database.", err);
      process.exit();
    });
};
