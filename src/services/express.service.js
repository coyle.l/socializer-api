const express = require("express");
const apiRouter = require("../routes/");
const config = require("../configs/");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

app.use(bodyParser.json());

app.use(cors());

app.use("/api", apiRouter);

exports.start = env => {
  let port;

  if (env == "test") {
    port = config.server.portTest;
  } else if (env == "dev") {
    port = config.server.portDev;
  } else {
    port = config.server.portDev;
  }
  app.listen(process.env.PORT || port, err => {
    if (err) {
      console.log(`Error : ${err}`);
      process.exit(-1);
    }

    console.log(`app is running on port ${port}`);
  });
};
