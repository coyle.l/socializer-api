const Joi = require("joi");

module.exports = {
  create: {
    password: Joi.string().required(),
    email: Joi.string()
      .email()
      .required()
      .max(128)
      .min(4),
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    admin: Joi.string(),
    adresse: Joi.string(),
    telephone: Joi.number()
  }
};
