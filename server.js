require('dotenv').config();
const config = require('./src/configs');
const mongoose = require('./src/services/mongoose.service');
const app = require('./src/services/express.service');

app.start(process.env.APP_ENV);
mongoose.connect(process.env.APP_ENV);
