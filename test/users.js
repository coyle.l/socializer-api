const mongoose = require("mongoose");
// const User = require ('./src/models/user.model');
const chai = require("chai");
const chaihttp = require("chai-http");
const faker = require("faker");
chai.use(chaihttp);
const should = chai.should();
let userId = undefined;

describe("CRUD users", () => {
  for (var i = 1; i < 4; i++) {
    describe("Create user", () => {
      it("It should create a user", done => {
        chai
          .request("localhost:" + (process.env.PORT | 3000) + "/api")
          .post("/users")
          .send({
            email: faker.internet.email(),
            password: faker.internet.password(),
            firstname: faker.name.firstName(),
            lastname: faker.name.lastName(),
            admin: "true"
          })
          .end((err, res) => {
            console.log(res.body);
            res.should.have.status(200);
            res.body.should.be.a("object");
            userId = res.body._id;

            done();
          });
      });
    });
  }

  describe("Get users", () => {
    it("It should get all users", done => {
      chai
        .request("localhost:" + (process.env.PORT | 3000) + "/api")
        .get("/users")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          done();
        });
    });
  });

  describe("Get user", () => {
    it("It should get one user", done => {
      chai
        .request("localhost:" + (process.env.PORT | 3000) + "/api")
        .get(`/users/${userId}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  describe("update user", () => {
    it("It should update one user", done => {
      chai
        .request("localhost:" + (process.env.PORT | 3000) + "/api")
        .put(`/users/${userId}`)
        .send({
          email: faker.internet.email(),
          password: faker.internet.password(),
          firstname: faker.name.firstName(),
          lastname: faker.name.lastName(),
          admin: faker.random.boolean()
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  describe("delete one user", () => {
    it("It should delete one user", done => {
      chai
        .request("localhost:" + (process.env.PORT | 3000) + "/api")
        .delete(`/users/${userId}`)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe("delete all users", () => {
    it("It should remvove all users", done => {
      chai
        .request("localhost:" + (process.env.PORT | 3000) + "/api")
        .delete(`/users/`)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });
});
